import React, { Component } from 'react'; 
const CallAnimal = props => {
  return (
    <div>
      <h1>
        Hello {props.name} with {props.color} color
      </h1>
      <p>{props.children}</p>
    </div>
  );
};
export default CallAnimal;
