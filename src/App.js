import React, { Component } from "react";
import "./App.css";
import CallAnimal from "./Animals/CallAnimal";
class App extends Component {
  state = {
    animals: [
      { name: "Lion", color: "yellow and black" },
      { name: "cat", color: "white" },
    ],
  };
  render() {
    return (
      <header className="App-header">
        <h1>Hello World</h1>
        <CallAnimal
          name={this.state.animals[0].name}
          color={this.state.animals[0].color}
        ></CallAnimal>

        <CallAnimal
          name={this.state.animals[1].name}
          color={this.state.animals[1].color}
        ></CallAnimal>
      </header>
    );
  }
}

export default App;
